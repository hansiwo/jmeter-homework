/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 94.76707880604471, "KoPercent": 5.232921193955289};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.23821363679831692, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.05568181818181818, 500, 1500, "AddToCart"], "isController": true}, {"data": [0.0, 500, 1500, "SortElements"], "isController": true}, {"data": [0.3847826086956522, 500, 1500, "LogoutClick"], "isController": false}, {"data": [0.12777777777777777, 500, 1500, "SortByLatest"], "isController": false}, {"data": [0.3333333333333333, 500, 1500, "SortByLatest-1"], "isController": false}, {"data": [0.38181818181818183, 500, 1500, "RemoveCouponClick"], "isController": false}, {"data": [0.13784461152882205, 500, 1500, "ApplyAndRemoveCoupon"], "isController": true}, {"data": [0.35909090909090907, 500, 1500, "AddToCartClick"], "isController": false}, {"data": [0.1348314606741573, 500, 1500, "RemoveItem"], "isController": true}, {"data": [0.17604166666666668, 500, 1500, "OpenApplication"], "isController": false}, {"data": [0.361671469740634, 500, 1500, "RemoveFirstClick-0"], "isController": false}, {"data": [0.42452830188679247, 500, 1500, "SearchClick"], "isController": false}, {"data": [0.11296296296296296, 500, 1500, "Search"], "isController": true}, {"data": [0.16322314049586778, 500, 1500, "Logout"], "isController": true}, {"data": [0.3602409638554217, 500, 1500, "ApplyCouponClick"], "isController": false}, {"data": [0.05257510729613734, 500, 1500, "LoginClick"], "isController": false}, {"data": [0.3170028818443804, 500, 1500, "RemoveFirstClick-1"], "isController": false}, {"data": [0.2407152682255846, 500, 1500, "MyAccountClick"], "isController": false}, {"data": [0.236, 500, 1500, "LoginClick-1"], "isController": false}, {"data": [0.282, 500, 1500, "LoginClick-0"], "isController": false}, {"data": [0.010570824524312896, 500, 1500, "Login"], "isController": true}, {"data": [0.36195054945054944, 500, 1500, "HomeClick"], "isController": false}, {"data": [0.12407407407407407, 500, 1500, "SortByPriceHighToLow"], "isController": false}, {"data": [0.362962962962963, 500, 1500, "SortByPriceHighToLow-1"], "isController": false}, {"data": [0.3925925925925926, 500, 1500, "SortByPriceHighToLow-0"], "isController": false}, {"data": [0.1174785100286533, 500, 1500, "RemoveFirstClick"], "isController": false}, {"data": [0.387037037037037, 500, 1500, "SortByLatest-0"], "isController": false}, {"data": [0.26296296296296295, 500, 1500, "AboutUsClick"], "isController": false}, {"data": [0.3242009132420091, 500, 1500, "CartClick"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 8007, 419, 5.232921193955289, 2089.937804421128, 0, 11576, 1431.0, 5028.0, 5523.999999999997, 10376.040000000003, 14.82012071540815, 356.4868401615184, 9.135611965068048], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["AddToCart", 440, 0, 0.0, 5959.990909090911, 801, 16694, 3640.5, 15362.100000000002, 16152.85, 16448.72, 0.8259560441210702, 92.14845627172828, 1.471707163150722], "isController": true}, {"data": ["SortElements", 270, 0, 0.0, 6936.196296296297, 1956, 13617, 5653.0, 12943.5, 13237.85, 13535.79, 0.5297867706356852, 38.603081283393735, 1.4929513065866231], "isController": true}, {"data": ["LogoutClick", 230, 0, 0.0, 1943.269565217393, 380, 5936, 1245.0, 5015.0, 5291.65, 5805.82, 0.4526872895250129, 10.886506560521338, 0.24773634827988672], "isController": false}, {"data": ["SortByLatest", 270, 0, 0.0, 2715.0111111111128, 783, 5321, 2351.5, 4784.7, 4932.4, 5233.090000000003, 0.5310862484067413, 12.960653931292192, 0.5996906514805505], "isController": false}, {"data": ["SortByLatest-1", 270, 0, 0.0, 1424.4296296296293, 385, 2809, 1235.0, 2464.7, 2612.8999999999996, 2761.080000000001, 0.5314793234858745, 12.753802522336894, 0.2979632869584816], "isController": false}, {"data": ["RemoveCouponClick", 385, 0, 0.0, 1925.288311688312, 348, 5546, 1107.0, 5211.4, 5342.1, 5513.0599999999995, 0.7583442488944917, 0.4171104960507007, 0.520621100559402], "isController": false}, {"data": ["ApplyAndRemoveCoupon", 399, 0, 0.0, 3811.017543859653, 475, 11168, 2006.0, 10458.0, 10650.0, 10931.0, 0.785380506029124, 0.9158908871010601, 1.0625134710472135], "isController": true}, {"data": ["AddToCartClick", 440, 0, 0.0, 1905.2931818181796, 354, 5954, 1112.5, 5124.200000000002, 5345.6, 5695.209999999999, 0.8266543420019313, 5.8898754922350856, 0.5659709061117938], "isController": false}, {"data": ["RemoveItem", 356, 2, 0.5617977528089888, 3750.6207865168553, 0, 11526, 2690.0, 10540.7, 11041.099999999999, 11426.33, 0.7004688812764275, 53.70468880354111, 0.7965608435189904], "isController": true}, {"data": ["OpenApplication", 480, 0, 0.0, 2555.6354166666665, 537, 7649, 1854.5, 5687.5, 6090.299999999999, 7196.21, 0.9000528781065887, 21.591758017736666, 0.2909533728075274], "isController": false}, {"data": ["RemoveFirstClick-0", 347, 0, 0.0, 1800.4466858789622, 339, 5813, 1240.0, 5280.2, 5519.2, 5734.599999999999, 0.6833156760886774, 0.33825018264417567, 0.41906469197625923], "isController": false}, {"data": ["SearchClick", 265, 1, 0.37735849056603776, 1274.5433962264142, 0, 4872, 900.0, 2504.2000000000003, 2726.4999999999995, 4634.119999999997, 0.5215837250131873, 9.886133598258501, 0.28819730454388], "isController": false}, {"data": ["Search", 270, 1, 0.37037037037037035, 2968.359259259259, 588, 7704, 2331.5, 5597.6, 5884.799999999999, 7611.0700000000015, 0.5306468191653909, 25.614760328205058, 0.5831510667966424], "isController": true}, {"data": ["Logout", 242, 0, 0.0, 3433.2479338842986, 420, 11033, 2238.0, 9799.1, 10116.449999999999, 10750.96, 0.47586742569487456, 18.956617347039103, 0.5131578664375818], "isController": true}, {"data": ["ApplyCouponClick", 415, 0, 0.0, 1986.3783132530139, 342, 5721, 1064.0, 5292.2, 5420.6, 5618.919999999999, 0.8174875357773913, 0.5191495993572381, 0.5644176638619293], "isController": false}, {"data": ["LoginClick", 466, 188, 40.34334763948498, 3290.5922746781143, 349, 11576, 2253.5, 6881.900000000001, 10835.95, 11148.99, 0.8747970226865279, 15.462927968279221, 0.6429258739757271], "isController": false}, {"data": ["RemoveFirstClick-1", 347, 0, 0.0, 2047.449567723342, 367, 6340, 1438.0, 5321.0, 5589.0, 5884.319999999998, 0.6832578205453855, 53.397521279813766, 0.3781119643219306], "isController": false}, {"data": ["MyAccountClick", 727, 228, 31.361760660247594, 1801.814305364511, 353, 6278, 1193.0, 4779.0, 4969.4, 5710.520000000001, 1.365073642624847, 23.06441920939798, 0.5787032962209805], "isController": false}, {"data": ["LoginClick-1", 250, 0, 0.0, 2541.435999999998, 386, 6462, 1837.0, 5891.8, 6082.45, 6304.110000000001, 0.520172281059487, 9.001176971062815, 0.1823650868167537], "isController": false}, {"data": ["LoginClick-0", 250, 0, 0.0, 2114.467999999999, 349, 5548, 1503.5, 4841.6, 4993.45, 5462.590000000001, 0.5204169997335465, 0.5035745980038886, 0.1905823973633593], "isController": false}, {"data": ["Login", 473, 223, 47.145877378435515, 5174.226215644821, 724, 16432, 3272.0, 10448.400000000001, 15573.499999999998, 15976.779999999999, 0.8875345491118087, 30.412182572705593, 0.9548282090566051], "isController": true}, {"data": ["HomeClick", 728, 0, 0.0, 1769.7225274725288, 372, 5568, 1229.5, 4677.1, 4980.1, 5377.97, 1.3676318365153943, 32.89635668420667, 0.7471201123130073], "isController": false}, {"data": ["SortByPriceHighToLow", 270, 0, 0.0, 2827.8999999999996, 731, 6047, 2412.5, 5367.8, 5653.75, 5833.700000000001, 0.530966142059008, 12.959783493639222, 0.6056735852456309], "isController": false}, {"data": ["SortByPriceHighToLow-1", 270, 0, 0.0, 1482.3333333333342, 387, 3472, 1287.0, 2809.8, 2957.1, 3305.32, 0.5313820459783235, 12.750382269109581, 0.3009742680704337], "isController": false}, {"data": ["SortByPriceHighToLow-0", 270, 0, 0.0, 1345.6888888888877, 340, 3023, 1118.5, 2511.9, 2725.75, 2901.0700000000015, 0.531434341287134, 0.21957418454352742, 0.3052037724703233], "isController": false}, {"data": ["RemoveFirstClick", 349, 2, 0.5730659025787965, 3825.8481375358174, 1, 11526, 2713.0, 10554.0, 11053.0, 11428.5, 0.6866942656109809, 53.70458313390833, 0.7965592762016166], "isController": false}, {"data": ["SortByLatest-0", 270, 0, 0.0, 1290.3777777777777, 351, 3003, 1078.0, 2326.8, 2560.2999999999997, 2774.5700000000006, 0.5315640780808568, 0.21647854794412671, 0.30221940307322787], "isController": false}, {"data": ["AboutUsClick", 270, 0, 0.0, 1717.4185185185188, 480, 3563, 1460.5, 3015.8, 3215.0, 3364.660000000001, 0.5311081867376385, 15.756789671076833, 0.2956325510798216], "isController": false}, {"data": ["CartClick", 438, 0, 0.0, 2045.3105022831066, 421, 6144, 1247.5, 5539.1, 5663.5, 5958.650000000001, 0.823659719466765, 66.5138572990731, 0.45687375064172114], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user87&lt;\\\/strong&gt;\\\/", 19, 4.534606205250597, 0.23729236917697016], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user90&lt;\\\/strong&gt;\\\/", 18, 4.2959427207637235, 0.22480329711502436], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user84&lt;\\\/strong&gt;\\\/", 21, 5.011933174224343, 0.26227051330086176], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user88&lt;\\\/strong&gt;\\\/", 16, 3.818615751789976, 0.19982515299113277], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user85&lt;\\\/strong&gt;\\\/", 19, 4.534606205250597, 0.23729236917697016], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Username or email address\\\/", 228, 54.41527446300716, 2.847508430123642], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user82&lt;\\\/strong&gt;\\\/", 19, 4.534606205250597, 0.23729236917697016], "isController": false}, {"data": ["Non HTTP response code: org.apache.http.NoHttpResponseException\/Non HTTP response message: lifesciencejobs.pl:443 failed to respond", 3, 0.7159904534606205, 0.03746721618583739], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user83&lt;\\\/strong&gt;\\\/", 20, 4.77326968973747, 0.24978144123891594], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user86&lt;\\\/strong&gt;\\\/", 20, 4.77326968973747, 0.24978144123891594], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user81&lt;\\\/strong&gt;\\\/", 20, 4.77326968973747, 0.24978144123891594], "isController": false}, {"data": ["Test failed: text expected to contain \\\/Hello &lt;strong&gt;user89&lt;\\\/strong&gt;\\\/", 16, 3.818615751789976, 0.19982515299113277], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 8007, 419, "Test failed: text expected to contain \\\/Username or email address\\\/", 228, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user84&lt;\\\/strong&gt;\\\/", 21, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user83&lt;\\\/strong&gt;\\\/", 20, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user86&lt;\\\/strong&gt;\\\/", 20, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user81&lt;\\\/strong&gt;\\\/", 20], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["SearchClick", 265, 1, "Non HTTP response code: org.apache.http.NoHttpResponseException\/Non HTTP response message: lifesciencejobs.pl:443 failed to respond", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["LoginClick", 466, 188, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user84&lt;\\\/strong&gt;\\\/", 21, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user83&lt;\\\/strong&gt;\\\/", 20, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user86&lt;\\\/strong&gt;\\\/", 20, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user81&lt;\\\/strong&gt;\\\/", 20, "Test failed: text expected to contain \\\/Hello &lt;strong&gt;user87&lt;\\\/strong&gt;\\\/", 19], "isController": false}, {"data": [], "isController": false}, {"data": ["MyAccountClick", 727, 228, "Test failed: text expected to contain \\\/Username or email address\\\/", 228, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["RemoveFirstClick", 349, 2, "Non HTTP response code: org.apache.http.NoHttpResponseException\/Non HTTP response message: lifesciencejobs.pl:443 failed to respond", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
